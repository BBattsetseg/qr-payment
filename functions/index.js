const functions = require("firebase-functions");
const admin = require("firebase-admin");
const axios = require("axios");
const qrcode = require("qrcode");
const cors = require("cors")({ origin: true });
const baseUrl =
  "https://us-central1-qr-payment-13a96.cloudfunctions.net/bank_payment";

admin.initializeApp();

//Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope CurrentUser
exports.shop_create_invoice = functions.https.onRequest((request, response) => {
  cors(request, response, async () => {
    console.log(request, response);
    const db = admin.firestore();
    functions.logger.log(request.body);

    const invoice = await db.collection("shop_invoices").add({
      name: request.body.name,
      description: request.body.description,
      price: request.body.price,
    });

    const res = await axios.post(
      "https://us-central1-qr-payment-13a96.cloudfunctions.net/bank_qr_generate",
      {
        id: invoice.id,
      }
    );

    const { qr, invoiceId } = res.data;

    response.send({
      qr,
      invoiceId,
    });
  });
});

exports.shop_recieve = functions.https.onRequest(async (request, response) => {
  const db = admin.firestore();
  functions.logger.log(request.body);
  const res = await db.doc(`shop_invoices/${request.body.id}`).set(
    {
      status: request.body.status,
    },
    {
      merge: true,
    }
  );

  response.send({
    'invoice-check': 'complete'
  });
});

//bank qr generate
exports.bank_qr_generate = functions.https.onRequest((request, response) => {
  qrcode.toDataURL(`${baseUrl}?invoiceId=${request.body.id}`, (err, code) => {
    if (err) return console.log("error occurred");

    response.send({
      qr: code,  // base64 bolgoj bga code
      invoiceId: request.body.id, 
    });
  });
});

exports.bank_payment = functions.https.onRequest((request, response) => {
  cors(request, response, async () => {
    const db = admin.firestore();
    const { invoiceId } = request.query;
    functions.logger.log("invoiceId", invoiceId);

     await db.doc(`bank_invoices/${invoiceId}`).set(
      {
        invoice_id : invoiceId,
        status: "paid",
      },
    );

    const res = await axios.post(
      "https://us-central1-qr-payment-13a96.cloudfunctions.net/shop_recieve",
      {
        status: "paid",
        id:invoiceId
      }
    );

    response.send({
      'invoice-status': 'to shop recieve'
    });
  });
});
