import firebase from "firebase/app";
import "firebase/firestore";
import  "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCbyXxDkEpkt5rrq_1ME1IM6QD9w-W2kxk",
    authDomain: "qr-payment-13a96.firebaseapp.com",
    projectId: "qr-payment-13a96",
    storageBucket: "qr-payment-13a96.appspot.com",
    messagingSenderId: "622593994278",
    appId: "1:622593994278:web:dcf2b1cc3d82ebe27bc0e7"
  };


firebase.initializeApp(firebaseConfig);

export const firestore = firebase.firestore();
export const auth = firebase.auth();
export default firebase;